﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartAudioOnLoad : MonoBehaviour {

    [SerializeField]
    private MusicControl m_MC;

    private void Start()
    {
        m_MC.StopAudio();
        m_MC.PlayAudio();
    }

}

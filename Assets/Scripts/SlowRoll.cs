﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SlowRoll : MonoBehaviour {

    [SerializeField]
    float m_rollingSpeed = 6000.0f;

	Rigidbody m_myRig;

	private void Start()
	{
		m_myRig = GetComponent<Rigidbody>();
        m_myRig.AddTorque(transform.up*m_rollingSpeed,ForceMode.Impulse);
	}
}

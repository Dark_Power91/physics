﻿using UnityEngine;

public delegate void OnLostWaste();

public class FireWallForTarget : MonoBehaviour 
{

    public event OnLostWaste onLostWaste = null;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Target")
        {
            Destroy(collision.gameObject);
            LostWaste();
        }
    }

    private void LostWaste()
    {
        if(onLostWaste != null)
        {
            onLostWaste();
        }
    }
}

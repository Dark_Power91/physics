﻿using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]

public class JointSetup : MonoBehaviour {

	Rigidbody m_myRig;
	LineRenderer m_myRenderer;
    bool RPressed;

	void Start () {
		m_myRig = GetComponent<Rigidbody>();
		GetComponent<ConfigurableJoint>().connectedBody = transform.parent.GetComponent<Rigidbody>();
	}

	private void Update()
	{
        RPressed = Input.GetKey(KeyCode.R);
	}

    private void FixedUpdate()
    {
        if(RPressed)
        {
            m_myRig.velocity = Vector3.zero;
            m_myRig.angularVelocity = Vector3.zero;
            m_myRig.ResetInertiaTensor();
        }
    }
}

﻿using UnityEngine;

public delegate void OnCollectedWaste();

public class CollectWaste : MonoBehaviour 
{
    public OnCollectedWaste onCollectedWaste = null;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Target")
        {
            Destroy(collision.gameObject);
            CollectedWaste();
        }
    }

    private void CollectedWaste()
    {
        if(onCollectedWaste!=null)
        {
            onCollectedWaste();
        }
    }
}

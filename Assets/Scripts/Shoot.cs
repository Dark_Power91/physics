﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    [Header("Raycast Option")]
    [SerializeField]
    private Transform m_RaycastStart;
    [SerializeField]
    private float m_RaycastLenght;
    [SerializeField]
    private float m_AttractStrength;

    [Header("Shoot Effect")]
    [SerializeField]
    private ParticleSystem m_ShootParticleEffect;
    [SerializeField]
    private Transform m_ParticleTarget;
    [SerializeField]
    private float m_ParticleOnScreeTime;

    void Update () {
        if(Input.GetButtonDown("Fire1"))
        {
            m_ParticleTarget.localPosition = Vector3.zero;
            LayerMask layerMask = 1 << LayerMask.NameToLayer("Target");
            RaycastHit hit = new RaycastHit();
            if(Physics.Raycast(m_RaycastStart.position, transform.forward, out hit, m_RaycastLenght, layerMask))
            {
                m_ParticleTarget.position = hit.point;
                hit.rigidbody.AddForce(-transform.forward * m_AttractStrength, ForceMode.Impulse);
            }
			if (!m_ShootParticleEffect.isEmitting)
			{
				StartCoroutine("PlayParticle");
			}
        }
    }

    private IEnumerator PlayParticle()
    {
        m_ShootParticleEffect.Play(true);
        yield return new WaitForSeconds(m_ParticleOnScreeTime);
        m_ShootParticleEffect.Stop(true);
    }
}

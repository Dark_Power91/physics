﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PositionIndicator : MonoBehaviour {

    [SerializeField]
    private Sprite m_Box;

    [SerializeField]
	private Sprite m_Arrow;

    private Quaternion m_StartRotation;
    private Image m_image;
    private void Start()
    {
        m_StartRotation = transform.rotation;
        m_image = GetComponent<Image>();

    }

    public void SetArrow(Color i_Color)
    {
        m_image.sprite = m_Arrow;
        SetColor(i_Color);
    }

    public void SetColor(Color i_Color)
    {
        m_image.color = i_Color;
    }

    public void SetBox(Color i_Color)
    {
        transform.rotation = m_StartRotation;
        m_image.sprite = m_Box;
        SetColor(i_Color);
    }
}

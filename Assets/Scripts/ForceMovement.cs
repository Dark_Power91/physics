﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ForceMovement : MonoBehaviour {

    [Header("Movement Forces")]
    [SerializeField]
    private float m_forceScale = 0.1f;
	[SerializeField]
	private float m_torqueScale = 0.01f;
    [Header("Particle Systems")]
    [SerializeField]
    private ParticleSystem m_EMP;

	Rigidbody m_myRig;

	float m_oldForce;
	float m_oldTorque;

	bool WPressed;
	bool APressed;
	bool SPressed;
	bool DPressed;
	bool QPressed;
	bool EPressed;
	bool RPressed;

	private void Start()
	{
		m_myRig = GetComponent<Rigidbody>();
	}

	void Update()
	{
		WPressed = Input.GetKey(KeyCode.W);
		SPressed = Input.GetKey(KeyCode.S);
		DPressed = Input.GetKey(KeyCode.D);
		APressed = Input.GetKey(KeyCode.A);
		QPressed = Input.GetKey(KeyCode.Q);
		EPressed = Input.GetKey(KeyCode.E);
		RPressed = Input.GetKey(KeyCode.R);
	}

	private void FixedUpdate()
	{

		if (WPressed)
		{
			m_myRig.AddForce(transform.forward * m_forceScale, ForceMode.Impulse);
			m_myRig.angularVelocity = Vector3.zero;
		}

		if (SPressed)
		{
			m_myRig.AddForce(transform.forward * -m_forceScale, ForceMode.Impulse);
			m_myRig.angularVelocity = Vector3.zero;
		}

		if (DPressed)
		{
			m_myRig.AddTorque(transform.up * m_torqueScale, ForceMode.Impulse);
		}

		if (APressed)
		{
			m_myRig.AddTorque(transform.up * -m_torqueScale, ForceMode.Impulse);
		}

		if (QPressed)
		{
			m_myRig.AddForce(transform.up * m_forceScale, ForceMode.Impulse);
		}

		if (EPressed)
		{
			m_myRig.AddForce(transform.up * -m_forceScale, ForceMode.Impulse);
		}

		if (RPressed)
		{
            if(!m_EMP.isEmitting)
            {
                StartCoroutine("ActivateEMP");
            }
			m_myRig.velocity = Vector3.zero;
			m_myRig.angularVelocity = Vector3.zero;
            m_myRig.ResetInertiaTensor();
		}
	}

    private IEnumerator ActivateEMP()
    {
        m_EMP.Play();
        yield return new WaitForSeconds(0.1f);
        m_EMP.Stop();
    }
}

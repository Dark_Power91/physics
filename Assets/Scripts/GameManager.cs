﻿using Random = UnityEngine.Random;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [SerializeField]
    private CollectWaste m_Player;

    [Header("Indicator preset")]
    [SerializeField]
    private GameObject[] m_TargetPrefab;
    [SerializeField]
    private Canvas m_ArrowCanvas;
    [SerializeField]
    private PositionIndicator m_ArrowPrefab;

    [Header("Level prefs")]
    [SerializeField]
    private int m_TargetQuantity = 1;
    [SerializeField]
    private float m_MaxDistance = 1;
    [SerializeField]
    private Transform m_UnitSphereCenter;

    [Header("UI Text")]
	[SerializeField]
	private TextMeshProUGUI m_ScoreText;
	[SerializeField]
	private string m_ScoreTextStart;
	[SerializeField]
	private string m_LostTextStart;
    [SerializeField]
    private string m_RemainingTextStart;

    [Header("End Game")]
    [SerializeField]
    private TextMeshProUGUI m_BackToShipText;
	[SerializeField]
	private GameObject m_EndPoint;
    [SerializeField]
    private TextMeshProUGUI m_GameOverText;
	[SerializeField]
    private Image m_FadeImage;
	[SerializeField]
    private Color m_FadeColor;

    private int m_LostWaste;
    private int m_Score;
    private int m_RemainingWaste;

	void Start () {

        FireWallForTarget[] walls = FindObjectsOfType(typeof(FireWallForTarget)) as FireWallForTarget[];
        foreach (FireWallForTarget f in walls)
        {
            f.onLostWaste += LostWaste;
        }

        m_Player.onCollectedWaste += CollectedWaste;
        m_RemainingWaste = m_TargetQuantity;
        for (int i = 0; i < m_TargetQuantity;i++)
        {
            GameObject temp = Instantiate(m_TargetPrefab[Random.Range(0, m_TargetPrefab.Length)], m_UnitSphereCenter.position+(Random.insideUnitSphere * m_MaxDistance), Random.rotation);
            PositionIndicator pI = Instantiate(m_ArrowPrefab, Vector3.zero, Quaternion.identity);
            pI.transform.SetParent(m_ArrowCanvas.transform);
            Target t = temp.AddComponent<Target>();
            t.SetUp(pI, Random.ColorHSV(0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 1.0f));
        }

        UpdateTexts();
	}

    private void LostWaste()
    {
        ++m_LostWaste;
        UpdateRemainigWaste();
        UpdateTexts();
    }

    private void CollectedWaste()
    {
        ++m_Score;
        UpdateRemainigWaste();
        UpdateTexts();
    }

    private void UpdateRemainigWaste()
    {
		if (--m_RemainingWaste <= 0)
		{
			m_BackToShipText.gameObject.SetActive(true);
			StartLevelEnd();
		}
    }

    private void UpdateTexts()
    {
        m_ScoreText.text = m_RemainingTextStart + m_RemainingWaste + "\n" + m_ScoreTextStart + m_Score + "\t\t" + m_LostTextStart + m_LostWaste;
    }

    private void StartLevelEnd()
    {
        EndLevelPoint eLP = m_EndPoint.AddComponent<EndLevelPoint>();
        eLP.SetUp(m_FadeImage, m_FadeColor, m_GameOverText);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnCameraSet();

public class CameraPositionMove : MonoBehaviour {
    
    [SerializeField]
    private Transform m_StartPoint;
	[SerializeField]
	private Transform m_EndPoint;
	[SerializeField]
	private float m_ScrollSpeed;

    private float m_TimeAccumulator;
    private Vector3 m_newPosition;

    public event OnCameraSet onCameraSet = null;

    void Start () 
    {
        transform.position = m_StartPoint.position;
        m_TimeAccumulator = 0.0f;
    }
	

	void Update () 
    {
        if (Input.anyKeyDown || m_TimeAccumulator >= 1f)
        {
            transform.position = m_EndPoint.position;
            CameraSet();
        }
        else
        {
			m_TimeAccumulator += Time.deltaTime * m_ScrollSpeed;
			m_newPosition.x = Mathf.Lerp(m_StartPoint.position.x, m_EndPoint.position.x, m_TimeAccumulator);
			m_newPosition.y = Mathf.Lerp(m_StartPoint.position.y, m_EndPoint.position.y, m_TimeAccumulator);
			m_newPosition.z = Mathf.Lerp(m_StartPoint.position.z, m_EndPoint.position.z, m_TimeAccumulator);
			transform.position = m_newPosition;
        }
	}

    void CameraSet()
    {
        if(onCameraSet !=null)
        {
            onCameraSet();
        }
		GetComponent<CameraPositionMove>().enabled = false;
    }
}

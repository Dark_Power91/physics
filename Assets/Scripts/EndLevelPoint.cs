﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Collider))]
public class EndLevelPoint : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI m_GameOverText;
	[SerializeField]
	private Image img;
	[SerializeField]
	private Color fadeColor;

    public void SetUp(Image i_img, Color i_fadeColor, TextMeshProUGUI i_GOText)
    {
        img = i_img;
        fadeColor = i_fadeColor;
        m_GameOverText = i_GOText;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            
            StartCoroutine(FadeImage(false));
        }
    }

    IEnumerator FadeImage(bool fadeAway)
    {
        m_GameOverText.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        img.gameObject.SetActive((true));
        if (fadeAway)
        {
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                fadeColor.a = i;
                img.color = fadeColor;
                yield return null;
            }
        }
        else
        {
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                fadeColor.a = i;
                img.color = fadeColor;
                yield return null;
            }
        }
        SceneManager.LoadScene(2);
    }

}
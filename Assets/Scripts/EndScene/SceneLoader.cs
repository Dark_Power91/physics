﻿using UnityEngine.SceneManagement;
using UnityEngine;


public class SceneLoader : MonoBehaviour {
    public void load(int level)
    {
        SceneManager.LoadScene(level);
    }

}

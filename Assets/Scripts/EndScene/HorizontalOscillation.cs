﻿using UnityEngine;

public class HorizontalOscillation : MonoBehaviour {

    [SerializeField]
    private float m_Amplitude = 0.5f;
    [SerializeField]
    private float m_Frequency = 1.0f;

    private Vector3 m_center;
	private float m_accumulator = 0.0f;
	private Vector3 m_newPos;

	void Start () 
    {
		m_center = transform.position;
		m_newPos = transform.position;
	}
	

	void Update () 
    {
		m_accumulator += Time.deltaTime;
        m_accumulator %= 360;
		m_newPos.y = transform.position.y;
        float sin = Mathf.Sin(2 * Mathf.PI * m_Frequency * m_accumulator);
		m_newPos.x = m_center.x + (m_Amplitude * sin);
		m_newPos.z = transform.position.z;
		transform.position = m_newPos;

        Quaternion newRotation = Quaternion.Euler(new Vector3(0, 0, 10.0f) * sin);
        transform.rotation = newRotation;

	}
}
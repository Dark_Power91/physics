﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    [SerializeField]
    private Color m_Color = Color.white;

    private PositionIndicator m_Arrow;
    private Camera mainCamera;

    public void SetUp (PositionIndicator i_Indicator, Color i_Color)
    {
        m_Arrow = i_Indicator;
        m_Color = i_Color;
        mainCamera = Camera.main;
    }
	
	void Update ()
    {
        Vector3 screenPos = mainCamera.WorldToScreenPoint(transform.position);

        if(screenPos.z > 0 &&
           screenPos.x > 0 && screenPos.x < Screen.width &&
           screenPos.y > 0 && screenPos.y < Screen.height)
        {
            m_Arrow.SetBox(m_Color);
            m_Arrow.transform.position = screenPos;
        }
        else
        {
            if(screenPos.z <0)
            {
                screenPos *= -1;
            }

            Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;
            screenPos -= screenCenter;

            float angle = Mathf.Atan2(screenPos.y, screenPos.x);
            angle -= 90 * Mathf.Deg2Rad;

            float cos = Mathf.Cos(angle);
            float sin = -Mathf.Sin(angle);

            screenPos = screenCenter + new Vector3(sin * 150, cos * 150, 0);

            float m = cos / sin;
            Vector3 screenBounds = screenCenter * 0.9f;

            if(cos>0)
            {
                screenPos = new Vector3(screenBounds.y / m, screenBounds.y, 0);
            }
            else
            {
                screenPos = new Vector3(-screenBounds.y / m, -screenBounds.y, 0);   
            }

            if(screenPos.x > screenBounds.x)
            {
                screenPos = new Vector3(screenBounds.x, screenBounds.x * m, 0);
            }
            else if(screenPos.x<-screenBounds.x)
            {
				screenPos = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);
            }

            screenPos += screenCenter;

            m_Arrow.SetArrow(m_Color);
            m_Arrow.transform.position = screenPos;
            m_Arrow.transform.rotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg);
        }
    }

    private void OnDestroy()
    {
        if(m_Arrow!=null){
			Destroy(m_Arrow.gameObject);         
        }
    }
}

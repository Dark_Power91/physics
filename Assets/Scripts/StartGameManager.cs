﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGameManager : MonoBehaviour {


    [SerializeField]
    private GameObject m_button;
    [SerializeField]
    private Image img;
    [SerializeField]
    private Color fadeColor;

    void OnEnable () 
    {
        FindObjectOfType<CameraPositionMove>().onCameraSet += ActivateButton;
	}

    private void ActivateButton()
    {
        m_button.SetActive(true);
    }

    public void LoadScene()
    {
        m_button.SetActive(false);
        StartCoroutine(FadeImage(false));
    }

    IEnumerator FadeImage(bool fadeAway)
    {
        if (fadeAway)
        {
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                fadeColor.a = i;
                img.color = fadeColor;
                yield return null;
            }
        }
        else
        {
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                fadeColor.a = i;
                img.color = fadeColor;
                yield return null;
            }
        }
        SceneManager.LoadScene(1);
    }

}
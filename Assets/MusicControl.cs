﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicControl : MonoBehaviour {

    AudioSource m_audio;

    private void Awake()
    {
        m_audio = GetComponent<AudioSource>();
    }

    public void StopAudio()
    {
        m_audio.Stop();
    }

    public void PlayAudio()
    {
        m_audio.Play();
    }

    public void PauseAudio()
    {
        m_audio.Pause();
    }

}
